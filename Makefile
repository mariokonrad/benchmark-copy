.PHONY: all prepare build run clean
all: prepare build run

prepare:
	cmake -B build -DCMAKE_CXX_COMPILER=g++-10 -DCMAKE_BUILD_TYPE=Release .
	#cmake -B build -DCMAKE_CXX_COMPILER=g++-10 -DCMAKE_BUILD_TYPE=Debug .

build:
	cmake --build build -j 4
	strip -s build/benchmark-copy

run:
	build/benchmark-copy

clean:
	rm -fr build

