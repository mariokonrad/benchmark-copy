#include <benchmark/benchmark.h>
#include <array>
#include <numeric>
#include <cstring>


constexpr std::size_t N_min = 8;
constexpr std::size_t N_max = 1024;


class aggregate_with_setter
{
private:
	using value_type = uint8_t;

	std::array<value_type, N_max> data_;

public:
	void set(const void * buf, std::size_t size)
	{
		assert(buf);
		assert(size <= data_.size());

		std::copy_n(reinterpret_cast<const value_type *>(buf), size,
			begin(data_));
	}
};

class aggregate_exposing_data
{
public:
	using value_type = uint8_t;

private:
	std::array<value_type, N_max> data_;

public:
	value_type * data()
	{
		return data_.data();
	}

	std::size_t capacity() const
	{
		return data_.size();
	}
};


static void bench_aggregate_exposing_data(benchmark::State & state)
{
	auto x = aggregate_exposing_data{};

	std::array<uint8_t, N_max> comm_buf;
	std::iota(begin(comm_buf), end(comm_buf), 0);

	for (auto _ : state) {
		const std::size_t size = state.range(0);

		// copy communication buffer directly to data
		std::memcpy(x.data(), comm_buf.data(), size);

		benchmark::ClobberMemory();
	}
}

BENCHMARK(bench_aggregate_exposing_data)->RangeMultiplier(2)->Range(N_min, N_max);

static void bench_agregate_with_setter(benchmark::State & state)
{
	auto x = aggregate_with_setter{};

	std::array<uint8_t, N_max> comm_buf;
	std::iota(begin(comm_buf), end(comm_buf), 0);

	std::array<uint8_t, N_max> buf;

	for (auto _ : state) {
		const std::size_t size = state.range(0);

		// copy communication buffer to temporary buffer
		std::copy_n(begin(comm_buf), size, begin(buf));

		// use setter with temporary buffer
		x.set(buf.data(), size);

		benchmark::ClobberMemory();
	}
}

BENCHMARK(bench_agregate_with_setter)->RangeMultiplier(2)->Range(N_min, N_max);

static void bench_agregate_with_setter_memcpy(benchmark::State & state)
{
	auto x = aggregate_with_setter{};

	std::array<uint8_t, N_max> comm_buf;
	std::iota(begin(comm_buf), end(comm_buf), 0);

	std::array<uint8_t, N_max> buf;

	for (auto _ : state) {
		const std::size_t size = state.range(0);

		// copy communication buffer to temporary buffer
		std::memcpy(comm_buf.data(), buf.data(), size);

		// use setter with temporary buffer
		x.set(buf.data(), size);

		benchmark::ClobberMemory();
	}
}

BENCHMARK(bench_agregate_with_setter_memcpy)->RangeMultiplier(2)->Range(N_min, N_max);

BENCHMARK_MAIN();

