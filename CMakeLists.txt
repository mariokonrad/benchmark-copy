cmake_minimum_required(VERSION 3.18)
project(benchmark-copy)

set(CMAKE_EXPORT_COMPILE_COMMANDS TRUE)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)


include(FetchContent)

FetchContent_Declare(
	google-benchmark
	#GIT_REPOSITORY https://github.com/google/benchmark
	GIT_REPOSITORY $ENV{HOME}/local/repo/benchmark
	GIT_TAG v1.5.2
	GIT_SHALLOW TRUE
)

set(BENCHMARK_ENABLE_GTEST_TESTS FALSE CACHE BOOL "" FORCE)
set(BENCHMARK_ENABLE_TESTING     FALSE CACHE BOOL "" FORCE)

FetchContent_MakeAvailable(google-benchmark)


add_executable(${PROJECT_NAME})

target_sources(${PROJECT_NAME}
	PRIVATE
		main.cpp
)

target_link_libraries(${PROJECT_NAME}
	PRIVATE
		benchmark::benchmark
)

target_compile_options(${PROJECT_NAME}
	PRIVATE
		-Wall
		-Wextra
		-fno-omit-frame-pointer
		-flto
)

target_link_options(${PROJECT_NAME}
	PRIVATE
		-flto
)

