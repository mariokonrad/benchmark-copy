#!/bin/bash -e

for i in $(seq 0 $(( $(grep "processor" /proc/cpuinfo | wc -l) - 1 ))) ; do
	sudo cpufreq-set -c $i -g performance
done

